function findSmallestInteger() {
  var sum = 0;
  var n = 1;

  while (sum <= 10000) {
    sum += n;
    n++;
  }

  return n;
}
const smallestInteger = findSmallestInteger();
document.getElementById("result1").innerHTML =
  "Số nguyên dương nhỏ nhất là: " + smallestInteger;
// /////////////
function calculateSum() {
  const x = parseInt(document.getElementById("xInput").value);
  const n = parseInt(document.getElementById("nInput").value);
  let sum = 0;

  for (let i = 1; i <= n; i++) {
    sum += Math.pow(x, i);
  }

  document.getElementById("result2").innerHTML = "Tổng S(n) = " + sum;
}
/////////////////
function calculateFactorial() {
  const n = parseInt(document.getElementById("nInput").value*1);

  if (n < 0) {
    document.getElementById("result").innerHTML =
      "Không tồn tại giai thừa cho số âm!";
  } else {
    let factorial = 1;
    for (let i = 1; i <= n; i++) {
      factorial = factorial * i;
    }

    document.getElementById("result3").innerHTML = n + "! = " + factorial;
  }
}
// ////
function createDivs() {
  const container = document.getElementById("container");
  container.innerHTML = ""; 

  for (let i = 1; i <= 10; i++) {
    const div = document.createElement("div");
    div.textContent = "Div " + i;

    if (i % 2 === 0) {
      div.className = "even";
    } else {
      div.className = "odd";
    }

    container.appendChild(div);
  }
}
